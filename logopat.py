import logging

class IndentFilter(logging.Filter):
    def __init__(self, context_manager):
        self.context_manager = context_manager

    def filter(self, record):
        record.indent = self.context_manager.indent
        record.indent_str = "----" * self.context_manager.indent +">"
        return True

class TreeLogger(logging.Logger):
    def __init__(self, name):
        self.indent = 0
        logging.Logger.__init__(self, name)
        self.addFilter(IndentFilter(self))
    
    def debug(self, *args, **kwargs):
        logging.Logger.debug(self, *args, **kwargs)
        return self

    def info(self, *args, **kwargs):
        logging.Logger.info(self, *args, **kwargs)
        return self

    def warning(self, *args, **kwargs):
        logging.Logger.warning(self, *args, **kwargs)
        return self

    warn = warning

    def error(self, *args, **kwargs):
        logging.Logger.error(self, *args, **kwargs)
        return self

    def __enter__(self):
        self.indent += 1

    def __exit__(self, typ, value, tb):
        self.indent -= 1



logging.basicConfig(level=logging.DEBUG,
    format="%(name)s %(levelname)-8s %(indent_str)s %(message)s")
logging.setLoggerClass(TreeLogger)
log = logging.getLogger("main")

def HeavyFunction(n):
    for i in range(n):
        log.debug("Interation %d", i)

def Init():
    log.info("Initializing some random stuff")
    log.debug("variable 1")
    log.debug("variable 2")
    log.debug("huh?")

def Main():
    for x in range(4):
        with log.debug("Request %d" %x):
            HeavyFunction(3)

if __name__ == "__main__":
    log.info("first")
    with log.info("Init") as _:
        Init()
    with log.info("Main") as _:
        Main()
