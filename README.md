# Tree logging with Logopat

What is Logopat? It is a proof-of-concept Python logging module supporting non-linear logs. Why would you ever want to use it? Well, if you ever tried to debug something inside a tight loop with >2 iterations, you should know that debugging can be a hell. Wouldn't it be nice to have some structure in it? And maybe even hide big portions of log which you don't need right now

# Example usage:

```python
def HeavyFunction(n):
    for i in range(n):
        log.debug("Interation %d", i)

def Init():
    log.info("Initializing some random stuff")
    log.debug("variable 1")
    log.debug("variable 2")
    log.debug("huh?")

def Main():
    for x in range(4):
        with log.debug("Request %d" %x):
            HeavyFunction(3)

logging.basicConfig(level=logging.DEBUG, format="%(name)s %(levelname)-8s %(indent_str)s %(message)s")
logging.setLoggerClass(TreeLogger)
log = logging.getLogger("main")

log.info("first")
with log.info("Init") as _:
    Init()
with log.info("Main") as _:
    Main()
```

with the output
```
main INFO     > first
main INFO     > Init
main INFO     ----> Initializing some random stuff
main DEBUG    ----> variable 1
main DEBUG    ----> variable 2
main DEBUG    ----> huh?
main INFO     > Main
main DEBUG    ----> Request 0
main DEBUG    --------> Interation 0
main DEBUG    --------> Interation 1
main DEBUG    --------> Interation 2
main DEBUG    ----> Request 1
main DEBUG    --------> Interation 0
main DEBUG    --------> Interation 1
main DEBUG    --------> Interation 2
main DEBUG    ----> Request 2
main DEBUG    --------> Interation 0
main DEBUG    --------> Interation 1
main DEBUG    --------> Interation 2
main DEBUG    ----> Request 3
main DEBUG    --------> Interation 0
main DEBUG    --------> Interation 1
main DEBUG    --------> Interation 2
```

# How can this be useful?
On its own, Logopat output is not very useful although it improves log readability a bit. However, you can use the indentation information to create an html webpage with collapsible children. But this is not implemented yet :-(
